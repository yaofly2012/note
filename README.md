**领导自己**
1. 保持好奇心；
2. 不分你我，不分边界；
3. 坚持不懈；
4. Done is done。

**何为靠谱**
- 凡事有交代；
- 件件有着落；
- 事事有回应。

## [**Buy me a coffee ☕**](http://yaofly2012.github.io/donate/)

